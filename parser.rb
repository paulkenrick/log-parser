def parse_input(input=nil)
  visits = {}

  input.split("\n").each do |line|
    webpage = line.split(" ").first
    ip = line.split(" ").last

    visits[webpage] = [] if visits[webpage].nil?
    visits[webpage] << ip
  end

  formatted_output = "\n*** Frequency of Page Views ***\n\n"
  visits.sort_by{ |webpage, ips| ips.length }.reverse.each do |webpage, ips|
    formatted_output += "#{webpage} #{ips.length} visits\n"
  end

  formatted_output += "\n*** Frequency of UNIQUE Page Views ***\n\n"
  visits.sort_by{ |webpage, ips| ips.uniq.length }.reverse.each do |webpage, ips|
    formatted_output += "#{webpage} #{ips.uniq.length} visits\n"
  end

  formatted_output
end

if $0 == __FILE__
  puts parse_input(File.read(ARGV[0]))
end
