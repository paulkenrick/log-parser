require_relative '../parser'

RSpec.describe 'Output', type: :feature do

  it 'lists webpages with most page views ordered from most pages views to less page views' do
    sample_input = File.read("webserver.log")
    sample_output = "/about/2 90 visits\n/contact 89 visits\n/index 82 visits\n/about 81 visits\n/help_page/1 80 visits\n/home 78 visits"
    expect(parse_input(sample_input)).to include(sample_output)
  end

  it 'lists webpages with most unique page views ordered from most pages views to less page views' do
    sample_input = File.read("webserver.log")
    sample_output = "/index 23 visits\n/home 23 visits\n/contact 23 visits\n/help_page/1 23 visits\n/about/2 22 visits\n/about 21 visits"
    expect(parse_input(sample_input)).to include(sample_output)
  end

end
