# Log Parser

This Ruby script accepts a webserver log file as an argument, and parses that file outputing two lists:

1. A list of the webpages and the number of views for each, ordered from most views to least views
2. A list of the webpages and the number of *unique* views for each, ordered from most views to least views

## Getting Started

To run the script simply run `ruby parser.rb webserver.log`.

## Tests

I have included simple feature specs to test that the output contains the desired lists.  I have used the RSpec framework to do this, using as input file at `log_parser/spec/support/files/webserver.log`.

To execute the tests simply run `rspec` from the root directory (note: RSpec must be installed to run the tests.  RSpec is bundled into the project so simply install with bundler by running `bundle install`).

## Notes from the Author

The code is contained in one single script (this seemed to be the request).  I have however included the important code in a method, so that it can be tested easily.

Given time constraints, I have made no attempt at formatting the output nicely (though I'd love to do this!).

This challenge took me about 2 hours.
